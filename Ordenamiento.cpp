#include <iostream>
#include <fstream>
#include "Ordenamiento.h"

Ordenamiento::Ordenamiento(){}

// imprime el array ordenado
void Ordenamiento::imprimir(int *arreglo, int largo) {
	
	int i;
	for (i = 0; i < largo; ++i) {
		cout << "array["<< i << "]=" << arreglo[i] << " ";
	}
	cout << endl;

}





// Método de selección 
void Ordenamiento::seleccion(int* arreglo, int largo) {
	clock_t start = clock();
	double tiempoOrndenar;
	int menor;
	int k; //para realizar los intercambios
	int i;
	int j;
	
	/* PASOS: Busca el elemento más pequeño de la lista.
	 Lo intercambia con el elemento ubicado en la primera posición de la lista.
	 Busca el segundo elemento más pequeño de la lista.
	 Lo intercambia con el elemento que ocupa la segunda posición en la lista.
	 Repite este proceso hasta que haya ordenado toda la lista. */

	for (i = 0; i < largo -1 ; ++i) {
		//la info contenida en la posicion i se guarda en un int "menor"
		menor = arreglo[i];
		k = i;
		//Segundo ciclo for iguala j a i+1 y se hace j+1, hasta que j alcanza al
		// tamaño
		for(j = i + 1; j < largo; ++j){
			if(arreglo[j] < menor){
				//reemplaza la info de menor, con la contenida en la posicion j
				menor = arreglo[j];
				k = j; //iguala variables
			}
		}
		//se reemplaza la info en la posicion k por la que esta en la posicion i
		arreglo[k] = arreglo[i];
		//posicion i se llena con la info contenida en menor
		arreglo[i] = menor;
	}
	tiempoOrndenar = ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000;
	cout << " Seleccion         |" << tiempoOrndenar << " milisegundos" << endl;
}



// Inicio Quicksort 
void Ordenamiento::reduceQckSort(int inicio, int fin, int &pos, int *arreglo){

	int temp;
	int izq = inicio;
	int der = fin;
	bool band = 1;

	pos = inicio;

	while (band) {
		while (arreglo[pos] <= arreglo[der] && pos != der) {
			der--;
		}

		if(pos == der) {
			band = 0;
		} else {
			temp = arreglo[pos];
			arreglo[pos] = arreglo[der];
			arreglo[der] = temp;
			pos = der;

			while (arreglo[pos] >= arreglo[izq] && pos != izq) {
				izq++;
				temp = arreglo[pos];
				arreglo[pos] = arreglo[izq];
				arreglo[izq] = temp;
				pos = izq;
			}
		}
	}
}

void Ordenamiento::qckSort(int *arreglo, int largo) {

	clock_t start = clock();
	double tiempoOrndenar;
	int pilaMenor[largo];
	int pilaMayor[largo];
	int inicio;
	int fin;
	int pos;
	int tope = 0;
	pilaMenor[tope] = 0;
	pilaMayor[tope] = largo - 1;

	while(tope >=0 ){
		inicio = pilaMenor[tope];
		fin = pilaMayor[tope];
		tope--;
		this->reduceQckSort(inicio, fin, pos, arreglo);

		if (inicio < (pos - 1)) {
			tope++;
			pilaMenor[tope] = inicio;
			pilaMayor[tope] = pos - 1;
		}

		if(fin > (pos + 1)) {
			tope++;
			pilaMenor[tope] = pos+1;
			pilaMayor[tope] = fin;
		}
	}
	tiempoOrndenar = ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000;
	cout << " Quicksort         |" << tiempoOrndenar << " milisegundos" << endl;
	cout << " -----------------------------------------" << endl;

}
