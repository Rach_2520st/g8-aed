G8-AED



Programa que ordena un mismo conjunto de elementos utilizando los siguientes algoritmos de ordenamiento interno.
-Selección
-Quicksort

Entrega además los tiempos en milisegundos que llevó cada algoritmo en ordenar el mismo conjunto de datos inicial. Para calcular el tiempo, se utilizó la función clock() de la librerı́a time.h.
 
El programa recibe como parámetros de entrada el valor N (positivo y menor o igual que 1.000.000) que corresponde al número de elementos a ordenar.
 
-Se debe definir un vector de tamaño máximo 1.000.000. Y un parámetro VER que indicará si se muestra o no el contenido de los vectores.

- A partir del N ingresado se  genera de forma aleatoria los elementos del vector. Utilizando las funciones srand() y rand() de la libreria stdlib.h para generarlos.



# Obtención del Programa
Clonar repositorio, ingresar a la carpeta G8-aed y ejecutar:
```
make
./ejecutable argumento2 argumento3 
```
Donde argumento2 corresponde al largo del arreglo a ordenar, un número entero entre 0 y 1000000. Y argumento3 es la opción de visualización del programa, puede ser 'n' o 's'.


# Acerca de 
Para iniciar el programa es necesario el ingreso de tres argumentos. En caso de no ingresarlos se notificará al usuario que reinicie el programa. 

El primero corresponde a ``` ./ejecutable ```. Es obligatorio que el segundo parametro ingresado sea un número entero entre 0 y 1000000, en caso de ser 0 no habrá datos que ordenar pero de todas formas se notificará al usuario.

El tercer argumento o 'ver' hace referencia a la visualización del ordenamiento y contiene 2 opciones; n (que muestra el tiempo de cada método en ordenar el arreglo) y s (que ademas de mostrar los tiempos muestra los arreglos ordenado por posición).

Una vez realizadas estas verificaciones, e ingresado un comando correcto, se creará un arreglo (de largo indicado por el usuario) con números aleatorios entre 1 y 1000000, estaran desordenados y quizás repetidos. Luego, este contenido se copiará en otros 7 arreglos distintos, uno por cada método. Cada arreglo se ordenará dependiendo de el método al que corresponda y de medirá el tiempo que tarda en completar la tarea.

NOTA: En caso de elegir un vector de gran tamaño es probable que se demore más el programa en calcularlo. Puede esperar a que se termine de calcular con método o detener el programa y reiniciarlo con otro valor.


# Requisitos
- Sistema operativo Linux
- Herramienta de gestion de dependencias make (para Makefile)
- Compilador GNU C++ (g++)





# Autor
Rachell Aravena Martinez
