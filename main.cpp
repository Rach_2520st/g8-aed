#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "Ordenamiento.h"

using namespace std;


void printArreglos(int* arreglo, int* arreglo1, int* arreglo2, 
	Ordenamiento orden,	int largo) {	

	// MOSTRAR VECTORES 
	cout << " Original Ordenado |";
	for (int i = 0; i < largo; ++i){
		cout << "array[" << i << "]=" << arreglo[i] << " ";
	}
	cout << endl;
	
	
	cout << " Seleccion         ";
	orden.imprimir(arreglo1, largo);
	
	cout << " Quicksort         ";
	orden.imprimir(arreglo2, largo);
	cout << " ***************************************" << endl;
}

void llenar(int* arreglo, int largo, string ver) {
	// Inicializa objeto que contiene los metodos de ordenamiento interno
	Ordenamiento orden;
	//Ordenamiento orden = Ordenamiento();

	cout << " Arreglo Original  ";
	// Llenar arreglo 
	for (int i = 0; i < largo; ++i) {
		// con números aleatorios  
		int aleatorio = (rand() % 100000) + 1;
		arreglo[i] = aleatorio;
		cout << "array[" << i << "]=" << arreglo[i] << " ";
	}
	cout << endl;
	
	// crear arreglos de cada metodo
	int *arreglo1 = new int[largo];
	int *arreglo2 = new int[largo];
	
	// asigna los mismos números a todos los arreglos 
	arreglo1 = arreglo;
	arreglo2 = arreglo;
	

	//no ver
	// Tabla de tiempo 
	cout << endl;
	cout << " -----------------------------------------" << endl;
	cout << " Método            |Tiempo" << endl;
	cout << " -----------------------------------------" << endl;
	
	
	orden.seleccion(arreglo1, largo);
	orden.qckSort(arreglo2, largo);
	
	//ver == "s"
	if (ver == "s") {
		// Imprimir arreglos ordenados 
		printArreglos(arreglo, arreglo1, arreglo2, orden, largo);
	}
}

int main(int argc, char *argv[]) {

	// Valida ingreso de los 3 argumentos 
	if (argc != 3) {
		// no ingresó los 3 argumentos 
		cout << "ERROR. Número de argumentos inválido" << endl;
		cout << " Ingresa el largo del arreglo y su visualizacion" << endl;
		cout << " Por favor reinicia el programa" << endl;
		exit(1);

	} else { //ingresó 3 argumentos
		
		/* Comprobar que el segundo sea un entero */
		try{
			int largo = stoi(argv[1]); //segundo argumento es un número
			string ver = argv[2]; //tercer argumento es 

			if (largo == 0) {
				cout << "No hay nada que ordenar" << endl;
				return 0;

			} else if (largo > 1000000 or largo <= 1) {
				/* Verificar rango del largo */
				cout << "ERROR. Número inválido." << endl;
				cout << " Ingrese un valor entre 0 y 1000000" << endl;
				cout << " Reinicie el programa" << endl;
				exit(1);

			} else { //2do parametro es válido
				
				// Comprobar que el tercero 
				if (ver == "s" or ver == "n"){
					// Verificar las 2 opciones (s o n)
					//tercer argumento es válido
					cout << " Generando arreglo" << endl;//generar array
					
					//si el parámetro es válido, se crea el arreglo
					int *arreglo = new int[largo];
					
					llenar(arreglo, largo, ver);

				} else { 
					cout << "ERROR. Tercer argumento inválido" << endl;
					cout << " Tercer argumento debe ser de tipo char. Corresponde";
					cout << " a si quieres ver o no el ordenamiento quicksort y selección ('n' o 's')" << endl;
					cout << " Reinicia el programa" << endl;
					exit(1);
				}
			}
		} catch (const std::invalid_argument& e){
			// segundo argumento no es un número
			cout << "ERROR. Segundo argumento inválido" << endl;
			cout << " Segundo argumento debe ser de tipo int con rango";
			cout << "  entre 0 y 1000000" << endl;
			cout << " Reinicia el programa" << endl;
			exit(1);
		}
	}
	return 0;
}
